/* eslint-disable linebreak-style */
/* eslint-disable no-undef */
/* eslint-disable linebreak-style */
/* eslint-disable no-unused-vars */
/* eslint-disable eqeqeq */
/* eslint-disable one-var */
/* eslint-disable semi */
/* eslint-disable no-mixed-spaces-and-tabs */
/* eslint-disable no-tabs */
/* eslint-disable no-trailing-spaces */
/* eslint-disable space-before-function-paren */
/* eslint-disable linebreak-style */
/* eslint-disable indent */
class Loot {
    constructor(arr, word) {
        this.arr = arr;
        this.word = word;
    }
    
    // Находит следующее значение на основе слова
    nextValue() {
        // находит номер колонки в луте
        var findColInLoot = function (b, a) {
            var col = -1;
            if (b == 0) {
                if (a == 0) {
                    col = 0;
                } else {
                    col = 1;
                }
            } else {
                if (a == 0) {
                    col = 2;
                } else {
                    col = 3;
                }
            }
            return col;
        }

        // находит номер строки в луте
        var findRowInLoot = function(d, c) {
            var row = -1;
            if (d == 0) {
                if (c == 0) {
                    row = 0;
                } else {
                    row = 1;
                }
            } else {
                if (c == 0) {
                    row = 2;
                } else {
                    row = 3;
                }
            }
            return row;
        }

        // нахождение следующего элемента
        let word = this.word;
        var a = word[0], b = word[1];
        var c = word[2], d = word[3];
        var col = findColInLoot(b, a);
        var row = findRowInLoot(d, c);
        return this.arr[row][col];
    }

    // Поиск следующего значения на основе слова и переданного значения
    nextValueWithTransfer(transferPartWord) {
        // находит номер колонки в луте
        var findColInLoot = function (b, a) {
        	var col = -1;
        	if (b == 0) {
        		if (a == 0) {
        			col = 0;
        		} else {
        			col = 1;
        		}
        	} else {
        		if (a == 0) {
        			col = 2;
        		} else {
        			col = 3;
        		}
        	}
        	return col;
        }

        // находит номер строки в луте
        var findRowInLoot = function (d, c) {
        	var row = -1;
        	if (d == 0) {
        		if (c == 0) {
        			row = 0;
        		} else {
        			row = 1;
        		}
        	} else {
        		if (c == 0) {
        			row = 2;
        		} else {
        			row = 3;
        		}
        	}
        	return row;
        }

        // находит индекс пустого места в слове
        var findIndexEmptyInWord = function(word) {
            for (let i = 0; i < word.length; i++) {
                if (word[i] == '') {
                    return i;
                }
            }
        }
        
        // Вставляет переданно слово на пустое место
        var insertTransferPart = function (word, transferPartWord) {
            word[findIndexEmptyInWord(word)] = transferPartWord;
            return word;
        }

        // нахождение следующего элемента
        let word = this.word;
        insertTransferPart(word, transferPartWord);
        var a = word[0],
        	b = word[1];
        var c = word[2],
        	d = word[3];
        var col = findColInLoot(b, a);
        var row = findRowInLoot(d, c);
        return this.arr[row][col];
    }

    // Проверка на переполнение нулем
    isOverflowZero() {
        var countZero = 0, 
            countOne = 0;
        for (let i = 0; i < this.arr.length; i++) {
            for (let j = 0; j < this.arr[i].length; j++) {
                switch (this.arr[i][j]) {
                    case 0:
                        countZero++;
                        break;
                    case 1:
                        countOne++;
                    	break;
                }
            }
        }
        if (countZero > countOne) {
            return true;
        } else {
            return false;
        }
    }

    // Проверка на переполнение единицой
    isOverflowOne() {
        var countZero = 0,
        	countOne = 0;
        for (let i = 0; i < this.arr.length; i++) {
        	for (let j = 0; j < this.arr[i].length; j++) {
        		switch (this.arr[i][j]) {
        			case 0:
        				countZero++;
        				break;
        			case 1:
        				countOne++;
        				break;
        		}
        	}
        }
        if (countOne > countZero) {
        	return true;
        } else {
        	return false;
        }
    }

    // Переставляем значения 
    swap() {
        console.log('Swap');
        console.log(this.word);
        console.log(this.arr);
        var a = this.word[0],
        	b = this.word[1];
        var c = this.word[2],
            d = this.word[3];
        // Определяем какая часть слова пуста
        if (a == '') {
            for (let i = 0; i < this.arr.length; i++) {
                let temp = 0;
                temp = this.arr[i][0];
                this.arr[i][0] = this.arr[i][2];
                this.arr[i][2] = temp;

                temp = this.arr[i][1];
                this.arr[i][1] = this.arr[i][3];
                this.arr[i][3] = temp;
            }
        } else if (b == '') {
            for (let i = 0; i < this.arr.length; i++) {
                let temp = 0;
                temp = this.arr[i][0];
                this.arr[i][0] = this.arr[i][2];
                this.arr[i][2] = temp;

                temp = this.arr[i][1];
                this.arr[i][1] = this.arr[i][3];
                this.arr[i][3] = temp;
            }
        } else if (c == '') {
            for (let i = 0; i < this.arr.length; i += 2) {
                for (let j = 0; j < this.arr[i].length; j++) {
                    let temp = 0;
                    temp = this.arr[i][j];
                    this.arr[i][j] = this.arr[i + 1][j];
                    this.arr[i + 1][j] = temp;
                }
            }
        } else {
            console.log('this.arr');
            console.log(this.arr);
            for (let i = 0; i < (this.arr.length / 2); i++) {
                for (let j = 0; j < this.arr[i].length; j++) {                 
                    let temp = 0;
                    temp = this.arr[i][j];
                    this.arr[i][j] = this.arr[i + 2][j];
                    this.arr[i + 2][j] = temp;
                }
            }
        }
    }
}

// Хранит массив лута
var loots = [];

// Парсит данные из таблиц и переводит их в бинарные значения
function parseLoots() {
    var tables = document.getElementsByClassName('table');
    var arrInputs = [];
    
    // достаем все инпуты из таблиц
    for (let i = 0; i < tables.length; i++) {
        arrInputs.push(tables[i].getElementsByTagName('input'));
    }

    // формируем двумерный массив лутов
    var arrLoot = [];
    var row = 0, col = 0;
    arrInputs.forEach(inputs => {
        for (let i = row, numInput = 0; i < row + 4; i++) {
            arrLoot[arrLoot.length] = []
            for (let j = col, m = 0; j < col + 4; j++, m++, numInput++) {
                if (inputs[numInput].checked) {
                    arrLoot[i][m] = 1;
                } else {
                    arrLoot[i][m] = 0;
                }
            }            
        }
        row += 4;
        col += 4;
    });
    return arrLoot;
}

// Парсит данные слова передаваемые для таблицы
/** номер строки соответствует новому слову следующей таблицы
 * намер столбца соответствует одному значению из всего слова
 */
function parseWords() {
    var wordInputs = document.getElementsByClassName('inputword');
    var numWord = 0;
    let arrWord = [];
    for (let row = 0; row < (wordInputs.length / 4); row++) {
        arrWord[row] = [];
        for (let col = 0; col < 4; col++) {
            arrWord[row][col] = wordInputs[numWord++].value;
        }
    }

    return arrWord;
}

// Сбрасывает глобальные переменные
function resetGlobalValues() {
    loots = [];
}

// Функция проверки заполненности перед началосм работы основного скрипта
function checkFullestWords() {
    let words = parseWords();
    for (let i = 0; i < words.length; i++) {
        let countEmpty = 1;
        for (let j = 0; j < words[i].length; j++) {
            if (i == 0) {
                if (words[i][j] == '') {
                    countEmpty--;
                }
                if (countEmpty <= 0) {
                    return false;
                }
            } else {
                if (words[i][j] == '') {
                    countEmpty--;
                }
                if (countEmpty < 0) {
                    return false;
                }
            }
        }
        if (i != 0 && countEmpty == 1) {
            return false;
        }
    }
    return true;
}

// Создает массив лутов
function createArrOfLoots(arrForLoots, words) {
    for (let i = 0, beginLoot = 0, endLoot = 3; i < words.length; i++, beginLoot += 4, endLoot += 4) {
    	// Отделение одного лута от массива лутов
    	let arrForLoot = [];
    	for (let j = beginLoot; j < endLoot + 1; j++) {
    		arrForLoot.push(arrForLoots[j]);
    	}
    	let loot = new Loot(arrForLoot, words[i]);
    	loots.push(loot);
    }
    return loots;
}

//скрипт запуска начала подсчета
function main() {
    hideResultRow()
    if (checkFullestWords()) {
        let words = parseWords();
        let arrForLoots = parseLoots();
        let loots = createArrOfLoots(arrForLoots, words);
        
        let previouslyValue = 0;
        if (document.getElementById('radioNoneOverflowed').checked) {
            // выполняем без проверки на переполнение
            for (let numLoot = 0; numLoot < loots.length; numLoot++) {
                if (numLoot == 0) {
                    previouslyValue = loots[numLoot].nextValue();
                } else {
                    previouslyValue = loots[numLoot].nextValueWithTransfer(previouslyValue);
                }
            }
            document.getElementById('pStringResult').innerHTML = previouslyValue;
            showResultRow();
        } else if (document.getElementById('radioOverflowedZero').checked) {
            // выполняем с проверкой на переполнение единицей
            for (let numLoot = 0; numLoot < loots.length; numLoot++) {
                if (numLoot == 0) {
                    previouslyValue = loots[numLoot].nextValue();
                } else {
                    if (loots[numLoot].isOverflowZero()) {
                        loots[numLoot].swap();
                    }
                    previouslyValue = loots[numLoot].nextValueWithTransfer(previouslyValue);
                }
            }
            guiUpdateLoot(loots);
            document.getElementById('pStringResult').innerHTML = previouslyValue;
            showResultRow();
        } else {
            // выполняем с проверкой на переполнение нулем
            for (let numLoot = 0; numLoot < loots.length; numLoot++) {
                if (numLoot == 0) {
                    previouslyValue = loots[numLoot].nextValue();
                } else {
                    if (loots[numLoot].isOverflowOne()) {
                        loots[numLoot].swap();
                    }
                    previouslyValue = loots[numLoot].nextValueWithTransfer(previouslyValue);
                }
            }
            guiUpdateLoot(loots);
            document.getElementById('pStringResult').innerHTML = previouslyValue;
            showResultRow();
        }
    } else {
        alert('Одно или несколько полей слов не заполненно или заполнено не верно');
    }
    resetGlobalValues()
}
