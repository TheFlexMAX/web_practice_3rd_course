/* eslint-disable linebreak-style */
/* eslint-disable no-undef */
/* eslint-disable linebreak-style */
/* eslint-disable no-unused-vars */
/* eslint-disable eqeqeq */
/* eslint-disable one-var */
/* eslint-disable semi */
/* eslint-disable no-mixed-spaces-and-tabs */
/* eslint-disable no-tabs */
/* eslint-disable no-trailing-spaces */
/* eslint-disable space-before-function-paren */
/* eslint-disable linebreak-style */
/* eslint-disable indent */
// создает копию первой таблицы и добавляет её в конец 
function addTable() {
  var interactZones = document.getElementsByClassName('interactionZone');
  var copyInteractZone = interactZones[0].cloneNode(true);
  document.getElementById('LootAndButton').appendChild(copyInteractZone);
}

// Создает таблицу лута в GUI
function createTable() {
  var table = document.createElement('table');
  table.className += 'table table-sm table-bordered text-center';
  for (let i = 0, count = 0; i < 4; i++) {
    var row = document.createElement('tr');
    for (let k = 0; k < 4; k++) {
      count++;
      var cell = document.createElement('td');

      var input = document.createElement('input');
      input.type = 'checkbox';
      input.className = 'form-check-input position-static big-checkbox';

      cell.appendChild(input);
      row.appendChild(cell);
    }
    table.appendChild(row);
  }
  // Добавление сформированной таблицы в html страницу
  document.getElementById('tableContainer').appendChild(table);
}

// удаление лута
function removeLoot() {
  var lootsCollection = document.getElementsByClassName('interactionZone');
  var loots = [];
  for (let i = 0; i < lootsCollection.length; i++) {
    loots.push(lootsCollection[i]);
  }
  var parent = document.getElementById('LootAndButton');

  if (loots.length > 1) {
    loots.pop();
    // Очистка дочерних элментов родителя
    while (parent.firstChild) {
      parent.removeChild(parent.firstChild);
    }
    // Перезапись оставшихся таблиц в документ
    for (let i = 0; i < loots.length; i++) {
      parent.appendChild(loots[i]);
    }
  }
}

// Перерисовывает значения в gui из класса дут в соответствующую таблицу
function guiUpdateLoot(loots) {
	// Получаем таблицы
	var tables = document.getElementsByClassName('table');
	// достаем все инпуты из таблиц
	var arrInputs = [];
	for (let i = 0; i < tables.length; i++) {
		arrInputs.push(tables[i].getElementsByTagName('input'));
	}
	
	let numArrInput = 0;
	loots.forEach(loot => {
		let numInput = 0;
		for (let i = 0; i < loot.arr.length; i++) {
			for (let j = 0; j < loot.arr[i].length; j++) {
				arrInputs[numArrInput][numInput++].checked = loot.arr[i][j];
			}
		}
		numArrInput++;
	});
	console.log('Обновление лутов выполнено');
}

/*
 **************************
 * 
 *    Управление словами
 * 
 **************************
 */

// очищает поля слов
function clearWords() {
  var wordInputs = document.getElementsByClassName('inputword');
  for (let i = 0; i < wordInputs.length; i++) {
    wordInputs[i].value = '';
  }
}

// сбрасывает состояние лутов
function resetLoots() {
  var inputs = document.getElementsByClassName('big-checkbox');
  for (let i = 0; i < inputs.length; i++) {
    inputs[i].checked = false;
  }
}

// добавить очистка полей ввода
function clearFields() {
  clearWords();
  resetLoots();
}

/*
 * 
 * Логика блока вывода
 * 
 */

// Отображает строку вывода
function hideResultRow() {
  document.getElementById('resultRow').style.display = 'none';
}

// Скрывает строку вывода
function showResultRow() {
	document.getElementById('resultRow').style.display = 'flex';
}
